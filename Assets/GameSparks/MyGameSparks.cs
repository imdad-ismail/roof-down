using System;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

namespace GameSparks.Api.Requests{
	public class LogEventRequest_condata : GSTypedRequest<LogEventRequest_condata, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_condata() : base("LogEventRequest"){
			request.AddString("eventKey", "condata");
		}
	}
	
	public class LogChallengeEventRequest_condata : GSTypedRequest<LogChallengeEventRequest_condata, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_condata() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "condata");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_condata SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
}
	

namespace GameSparks.Api.Messages {


}
