﻿using UnityEngine;
using System.Collections;

public class SceneRandomizer : MonoBehaviour {
    private SpriteRenderer frSprite, mSprite, bSprite;

    public static int SceneSelector;
    public GameObject front, middle, back;
    public Sprite desFront, desMiddle, desBack;
    public Sprite dumpFront, dumpMiddle, dumpBack;
    public Sprite forFront, forMiddle, forBack;
    public Sprite scaryFront, scaryMiddle, scaryBack;

    void Awake()
    {
        frSprite = front.GetComponent<SpriteRenderer>();
        mSprite  = middle.GetComponent<SpriteRenderer>();
        bSprite  = back.GetComponent<SpriteRenderer>();
        
    }
    // Use this for initialization
    void Start () {
        SceneSelector = 2;
        //Random.Range(0, 5);
        Debug.Log(SceneSelector);
        spriteChanger();
      
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void spriteChanger()
    {
        switch (SceneSelector)
        {
            case 1:
                frSprite.sprite = desFront;
                mSprite.sprite  = desMiddle;
                bSprite.sprite  = desBack;
                break;
            case 2:
                frSprite.sprite = dumpFront;
                mSprite.sprite  = dumpMiddle;
                bSprite.sprite  = dumpBack;
                break;
            case 3:
                frSprite.sprite = forFront;
                mSprite.sprite  = forMiddle;
                bSprite.sprite  = forBack;
                break;
            case 4:
                frSprite.sprite = scaryFront;
                mSprite.sprite  = scaryMiddle;
                bSprite.sprite  = scaryBack;
                break;

            default:
                frSprite.sprite = desFront;
                mSprite.sprite  = desMiddle;
                bSprite.sprite  = desBack;
                break;

        }
    }
 }
