﻿using UnityEngine;
using System.Collections;

public class SongManager : MonoBehaviour {
    private int songIndex;
    private AudioSource song1;
    private AudioSource song2;
    private AudioSource song3;
    private AudioSource song4;
    private AudioSource song5;
    private AudioSource song6;


    void Awake()
    {
        AudioSource[] audios = GetComponents<AudioSource>();
        song1 = audios[0];
        song2 = audios[1];
        song3 = audios[2];
        song4 = audios[3];
        song5 = audios[4];
        song6 = audios[5];
        songIndex = Random.Range(1, 6);
        switch (songIndex)
        {
            case 1:
                song1.Play();
                break;
            case 2:
                song2.Play();
                break;
            case 3:
               song3.Play();
                break;
            case 4:
               song4.Play();
                break;
            case 5:
               song5.Play();
                break;
            case 6:
                song6.Play();
                break;
        }
    }
    // Use this for initialization
    void Start () {
      
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (GameOverManager.gameover == true)
        {
            switch (songIndex)
            {
                case 1:
                    song1.Stop();
                    break;
                case 2:
                    song2.Stop();
                    break;
                case 3:
                    song3.Stop();
                    break;
                case 4:
                    song4.Stop();
                    break;
                case 5:
                    song5.Stop();
                    break;
                case 6:
                    song6.Stop();
                    break;
            }

        }
	}
}
