﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SceneChanger : MonoBehaviour {
    private CanvasGroup CG;
	// Use this for initialization
	void Start () {
        CG = GetComponent<CanvasGroup>();
        
    }
	
	// Update is called once per frame
	void Update () {
	if(CG.alpha < 0.001)
        {
            Debug.Log("ALPHA REACHED");
            Application.LoadLevel("StartScene");
        }
	}
}
