﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstaclePoolScript : MonoBehaviour
{

    public GameObject[] objectToSpawn;
    public static ObstaclePoolScript current;
    private int randomPick;
    public int noOfObjects = 10;
    public GameObject exclamation;

    private List<GameObject> objectList;
    private List<GameObject> exList;

    void Awake()
    {
        current = this;
        objectList = new List<GameObject>();
        exList = new List<GameObject>();
    }

    void Start()
    {
        for (int i = 0; i < noOfObjects; i++)
        {
            //int randomPick = Mathf.Abs(Random.Range (0, this.objectToSpawn.Length));
            GameObject instance = Instantiate(objectToSpawn[i]);
            instance.SetActive(false);
            objectList.Add(instance);

        }
        for (int i = 0; i < 5; i++)
        {
            //int randomPick = Mathf.Abs(Random.Range (0, this.objectToSpawn.Length));
            GameObject instance = Instantiate(exclamation);
            instance.SetActive(false);
            exList.Add(instance);
        }
    }

    public GameObject GetObject()
    {
        for (int i = 0; i < objectList.Count; i++)
        {

            if (!objectList[i].activeInHierarchy)
            {
                return objectList[i];
            }
        }
        int randomPick = Mathf.Abs(Random.Range(0, this.objectToSpawn.Length));
        GameObject obj = Instantiate(objectToSpawn[randomPick]);
    
        objectList.Add(obj);
        return obj;
    }

    public GameObject GetEx()
    {
        for (int i = 0; i < exList.Count; i++)
        {

            if (!exList[i].activeInHierarchy)
            {
                return exList[i];
            }
        }
        //int randomPick = Mathf.Abs(Random.Range(0, this.objectToSpawn.Length));
        GameObject obj = Instantiate(exclamation);

        exList.Add(obj);
        return obj;
    }
    public List<GameObject> GetListOfObjects(GameObject[] objectToSpawn, int amount)
    {
        List<GameObject> theList = new List<GameObject>();

        for (int i = 0; i < amount; i++)
        {
            int randomPick = Mathf.Abs(Random.Range(0, this.objectToSpawn.Length));
            GameObject obj = Instantiate(objectToSpawn[randomPick]);
            theList.Add(obj);
        }
        return theList;
    }
}