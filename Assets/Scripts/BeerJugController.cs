﻿using UnityEngine;
using System.Collections;

public class BeerJugController : MonoBehaviour
{

    public Sprite beer0, beer5, beer10, beer15, beer20, beer25, beer30, beer35, beer40, beer45, beer50, beer55, beer60, beer65, beer70, beer75, beer80, beer85, beer90, beer95, beer100;
    private SpriteRenderer spriteRenderer;
    private int lastInt;
    public float flashSpeed;
    private Color endColor;
    void Awake()
    {

    }
    // Use this for initialization
    void Start()
    {
        lastInt = DemoScene.beerTankCounter;
        spriteRenderer = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void LateUpdate()
    {       
        beerTank();
        if (lastInt != DemoScene.beerTankCounter)//Checking for changes in Beer value
        {
            if (DemoScene.beerTankCounter > 20)
            {
                DemoScene.beerTankCounter = 20;
            }
            StartCoroutine("WaitAndFlashGas");
            lastInt = DemoScene.beerTankCounter;
        }
    }

    void beerTank()
    {
        switch (DemoScene.beerTankCounter)
        {
            case 1:
                spriteRenderer.sprite = beer5;
                break;
            case 2:
                spriteRenderer.sprite = beer10;
                break;
            case 3:
                spriteRenderer.sprite = beer15;
                break;
            case 4:
                spriteRenderer.sprite = beer20;
                break;
            case 5:
                spriteRenderer.sprite = beer25;
                break;
            case 6:
                spriteRenderer.sprite = beer30;
                break;
            case 7:
                spriteRenderer.sprite = beer35;
                break;
            case 8:
                spriteRenderer.sprite = beer40;
                break;
            case 9:
                spriteRenderer.sprite = beer45;
                break;
            case 10:
                spriteRenderer.sprite = beer50;
                break;
            case 11:
                spriteRenderer.sprite = beer55;
                break;
            case 12:
                spriteRenderer.sprite = beer60;
                break;
            case 13:
                spriteRenderer.sprite = beer65;
                break;
            case 14:
                spriteRenderer.sprite = beer70;
                break;
            case 15:
                spriteRenderer.sprite = beer75;
                break;
            case 16:
                spriteRenderer.sprite = beer80;
                break;
            case 17:
                spriteRenderer.sprite = beer85;
                break;
            case 18:
                spriteRenderer.sprite = beer90;
                break;
            case 19:
                spriteRenderer.sprite = beer95;
                break;
            case 20:
                spriteRenderer.sprite = beer100;
                break;
            default:
                spriteRenderer.sprite = beer0;
                break;
        }
    }
    IEnumerator WaitAndFlashGas()
    {
        endColor = new Color(253, 168, 21, 1);
        for (int n = 0; n < 4; n++)
        {
            spriteRenderer.material.color = Color.white;
            yield return new WaitForSeconds(flashSpeed);
            spriteRenderer.material.color = endColor;
            yield return new WaitForSeconds(flashSpeed);
        }
        spriteRenderer.material.color = Color.white;

    }
}
