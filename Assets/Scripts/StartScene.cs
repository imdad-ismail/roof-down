﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartScene : MonoBehaviour {
    public GameObject infoPanel;
    public GameObject infoButt;
    public Text concertText;
    private string concertInfo;
	// Use this for initialization

	void Start () {
        infoPanel.SetActive(false);
        concertInfo = PlayerPrefs.GetString("ConcertInfo");
		//fix issues with åäö here
		concertText.text = GameSparksConnection.fixUmlauts(concertInfo);
		Invoke ("checkConcert", 5f);
    }
	
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

	// Update is called once per frame
	void checkConcert () {

        if(concertText.text != PlayerPrefs.GetString("ConcertInfo"))
        {
			string newtext=PlayerPrefs.GetString("ConcertInfo");
			Debug.Log("start "+newtext);
			newtext=GameSparksConnection.fixUmlauts(newtext);
			concertText.text = newtext;
			Debug.Log("fixed "+newtext);
			PlayerPrefs.SetString("ConcertInfo",newtext);
			//PlayerPrefs.GetString("ConcertInfo");
        }
    }

    public void getInformation()
    {
        infoButt.SetActive(false);
        infoPanel.SetActive(true);
    }
    public void remInformation()
    {
        infoButt.SetActive(true);
        infoPanel.SetActive(false);
    }
}
