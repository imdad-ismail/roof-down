﻿using UnityEngine;
using System.Collections;

public class SelectPlayer : MonoBehaviour {
     private int selectorIndex;
   // public GameObject johan, tobbe;
  //  public static SelectPlayer instance;
    public GameObject LoadingPanel;
    public static bool isblinkin;
    // Use this for initialization


    void Start () {
        
        Debug.Log("TimeScale is set to " + Time.timeScale);
        Time.timeScale = 1;
        selectorIndex = 0;
        LoadingPanel.SetActive(false);
    }
	void Awake()
    {
        isblinkin = true;

    }
	
    public void SelectJohan()
    {
        isblinkin = false;
        Time.timeScale = 1;
        LoadingPanel.SetActive(true);
        Invoke("LoadJohan", 0.5f);
    }

    public void SelectTobbe()
    {
        isblinkin = false;
        Time.timeScale = 1;
        LoadingPanel.SetActive(true);
        Invoke("LoadTobbe", 0.5f);
    }

    public void SelectStaffan()
    {
        isblinkin = false;
        Time.timeScale = 1;
        LoadingPanel.SetActive(true);
        Invoke("LoadStaffan", 0.5f);
    }

    public void SelectMats()
    {
        isblinkin = false;
        Time.timeScale = 1;
        LoadingPanel.SetActive(true);
        Invoke("LoadMats", 0.5f);
    }

    public void LoadMats()
    {
        selectorIndex = 4;
        PlayerPrefs.SetInt("PlayerSelection", selectorIndex);//Setting the character selected to user prefs
        Application.LoadLevel("DemoScene");
    }

    public void LoadStaffan()
    {
        selectorIndex = 3;
        PlayerPrefs.SetInt("PlayerSelection", selectorIndex);//Setting the character selected to user prefs
        Application.LoadLevel("DemoScene");
    }

    public void LoadTobbe()
    {
        selectorIndex = 2;
        PlayerPrefs.SetInt("PlayerSelection", selectorIndex);//Setting the character selected to user prefs
        Application.LoadLevel("DemoScene");
    }

    public void LoadJohan()
    {
        selectorIndex = 1;
        PlayerPrefs.SetInt("PlayerSelection", selectorIndex);//Setting the character selected to user prefs
        Application.LoadLevel("DemoScene");
    }
}
