﻿using UnityEngine;
using System.Collections;

public class BackgroundChanger : MonoBehaviour {
    private SpriteRenderer sr;
    public Sprite desert, dump, forest, scary;

	// Use this for initialization
	void Awake () {
        sr = GetComponent<SpriteRenderer>();
        Background();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void Background()// Used to change background image in the camera
    {
        switch (FloorSpawner.SceneSelector)
        {
            case 1:
                sr.sprite = desert;              
                break;
            case 2:
                sr.sprite = dump;               
                break;
            case 3:
                sr.sprite = forest;             
                break;
            case 4:
                sr.sprite = scary;               
                break;

            default:
                sr.sprite = desert;            
                break;

        }
    }

    
}
