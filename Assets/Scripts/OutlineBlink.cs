﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OutlineBlink : MonoBehaviour {
    private Image sr;
    public Sprite normal;
    public Sprite outline;
    public float flashSpeed;
    // Use this for initialization
    void Start () {
        sr = GetComponent<Image>();
        StartCoroutine("WaitAndFlash");
	}
    IEnumerator WaitAndFlash()
    {
        while (SelectPlayer.isblinkin == true)
        {
            
            sr.sprite = normal;
            yield return new WaitForSeconds(flashSpeed);
            sr.sprite = outline;
            yield return new WaitForSeconds(flashSpeed);
        }
    }

    // Update is called once per frame
 
}
