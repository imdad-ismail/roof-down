﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CloudPool : MonoBehaviour
{

    public GameObject[] objectToSpawn;
    public static CloudPool current;
    private int randomPick;
    public int noOfObjects = 10;

    private List<GameObject> objectList;

    void Awake()
    {
        current = this;
        objectList = new List<GameObject>();
    }

    void Start()
    {

        for (int i = 0; i < noOfObjects; i++)
        {
            // int randomPick = Mathf.Abs(Random.Range (0, this.objectToSpawn.Length));
            GameObject instance = (GameObject)Instantiate(objectToSpawn[i]);

            instance.SetActive(false);
            objectList.Add(instance);
        }
    }

    public GameObject GetObject()
    {
        for (int i = 0; i < objectList.Count; i++)
        {

            if (!objectList[i].activeInHierarchy)
            {
                return objectList[i];
            }
        }
        int randomPick = Mathf.Abs(Random.Range(0, this.objectToSpawn.Length));
        GameObject obj = Instantiate(objectToSpawn[randomPick]);

        objectList.Add(obj);

        return obj;
    }

    public List<GameObject> GetListOfObjects(GameObject[] objectToSpawn, int amount)
    {

        List<GameObject> theList = new List<GameObject>();

        for (int i = 0; i < amount; i++)
        {
            int randomPick = Mathf.Abs(Random.Range(0, this.objectToSpawn.Length));
            GameObject obj = (GameObject)Instantiate(objectToSpawn[randomPick]);
            theList.Add(obj);
        }
        return theList;
    }
}
