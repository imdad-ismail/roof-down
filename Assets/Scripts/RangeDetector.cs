﻿using UnityEngine;
using System.Collections;

public class RangeDetector : MonoBehaviour//Used to detect obstacles when player is near
{
    public static bool inCollider;//Check if play is in the Range detector collider
    private Vector3 upwards;
    private SpriteRenderer parentSprite;//Getting the sprite render of parent
    public Sprite Normal;
    public Sprite Feedback;


    // Use this for initialization
    void Start ()
    {
        inCollider = false;
        parentSprite = GetComponentInParent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        parentSprite.sprite = Feedback;
        if (other.gameObject.layer == 8)
        {//Player layer
            SpriteFlasher.doOnceE = false;
            inCollider = true;
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        
        if (DemoScene.swipeDetected == true ) // if touch on obstacle is detected
        {
            upwards = new Vector3(transform.position.x, +10, transform.position.z);
            StartCoroutine(MoveFromTo(transform.position, upwards, 1f));//Start lerping the object
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        parentSprite.sprite = Normal;
        inCollider = false;
        if (DemoScene.triggerStay == false)//Checking if human obstacle is evaded
        {
            transform.parent.gameObject.SetActive(false);// If not evaded set active false(explode)
        }
       
    }
    IEnumerator MoveFromTo(Vector3 start, Vector3 end, float time)
    {
        float t = 0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime / time; // Sweeps from 0 to 1 in time seconds
            transform.parent.position = Vector3.Lerp(start, end, t); // Set position proportional to t
            yield return 0;      // Leave the routine and return here in the next frame
        }
 
    }
}
