﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverManager : MonoBehaviour {
    public GameObject pauseButt;
    public GameObject infoButt;
    public GameObject gameOverPanel;
    private GameObject player;
    public static bool gameover;
    private bool DoOnce; 
    public Sprite gameWinImage;
    public Sprite gameLoseImage;
    public Text beerPicksText;
    public Text gasPicksText;
    public Text distTravelled;
    public Text gameOverText;
    public Text concertText;
    private string concertInfo;
    private float scoref;
    private int scorei;
    private Sprite gameOver;
    private AudioSource FailSound;
    private AudioSource WinSound;

    void Awake()
    {
        gameover = false;
        gameOverPanel.SetActive(false);
    }
	// Use this for initialization
	void Start () {
        
        AudioSource[] audios = GetComponents<AudioSource>();
        FailSound = audios[0];
        WinSound = audios[1];
        scoref = 0;
        scorei = 0;
        player = GameObject.FindGameObjectWithTag("Player");
        concertInfo = PlayerPrefs.GetString("ConcertInfo");
        DoOnce = true;
    }
	
	// Update is called once per frame
	void Update () {
        scoref = Time.timeSinceLevelLoad;
        scorei = (int)scoref;
        if (DemoScene.beerTankCounter <1 && DoOnce == true)
        {
            gameover = true;
            FailSound.Play();
            concertText.text = concertInfo;
            gameOverPanel.GetComponent<Image>().sprite = gameLoseImage;
            distTravelled.text = "Distance : \n" + scorei + "M";
            beerPicksText.text = ": " + DemoScene.countBeerPicks + " Beers";
            gasPicksText.text = ": " + DemoScene.countGasPicks + " Tanks";
            pauseButt.SetActive(false);
            infoButt.SetActive(false);
            gameOverText.text = "You Failed";
            gameOverPanel.SetActive(true);
            player.SetActive(false);
            Time.timeScale = 0;
            DoOnce = false;
        }

    if (DemoScene.GasTankCounter > 19 && DoOnce == true)
        {
            gameover = true;
            WinSound.Play();
            concertText.text = concertInfo;
            gameOverPanel.GetComponent<Image>().sprite = gameWinImage;
            distTravelled.text = "Distance : \n" + scorei + "M";
            beerPicksText.text = ": " + DemoScene.countBeerPicks + " Beers";
            gasPicksText.text = ": " + DemoScene.countGasPicks + " Tanks";
            infoButt.SetActive(false);
            pauseButt.SetActive(false);
            gameOverText.text = "You Win";
            gameOverPanel.SetActive(true);
            player.SetActive(false);
            Time.timeScale = 0;
            DoOnce = false;

        }
	}
  
}
