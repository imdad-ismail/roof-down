﻿using UnityEngine;
using System.Collections;

public class ExcDestroyer : MonoBehaviour {
    private SpriteRenderer sr;
    public float flashSpeed;
    bool DoOnce;
    // Use this for initialization
    void Start () {
        DoOnce = true;
        sr = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
        // if (isActiveAndEnabled)
        //{
        if (RangeDetector.inCollider == true && DoOnce == true)
        {
            Debug.Log("Coroutine Started");
            StartCoroutine("WaitAndFlashE");
            DoOnce = false;
        }
        // }
    }
    void LateUpdate()
    {
        if (RangeDetector.inCollider == false)
        {
            StopCoroutine("WaitAndFlashE");
         //   Debug.Log("SetActiveFALSEEE");
            sr.material.color = Color.white;
            gameObject.SetActive(false);
            DoOnce = true;
        }
    }
    IEnumerator WaitAndFlashE()
    {
        //Debug.Log("INSIDE COUROUTINE===============================");

        //for (int n = 0; n < 1; n++)
        //   {
        while (RangeDetector.inCollider == true)
        {
           // Debug.Log("Sprite material flashingggg");
            sr.material.color = Color.white;
            yield return new WaitForSeconds(flashSpeed);
            sr.material.color = Color.black;
            yield return new WaitForSeconds(flashSpeed);
            //    }
        }
        sr.material.color = Color.white;
    }

}
