﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GameSparks.Api.Requests;
using GameSparks.Core;

public class GameSparksConnection : MonoBehaviour {
    public static GameSparksConnection instance;
    private string concertInfo;
    private string retrievedConInfo;
    void Awake()
    {

        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }

        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start () {
        Invoke("ConnectGameSparks", 3f);
	}
	
	// Update is called once per frame

   void ConnectGameSparks()
    {
        Debug.Log("ConnectGameSparks");
        if (!GS.Authenticated)
        {
            Debug.Log("Not Authenticated");
            new DeviceAuthenticationRequest().Send((response) =>
            {
                if (response.HasErrors)
                {
                    Debug.Log("Something failed with connection");
                }
                else
                {
                    Debug.Log(response.UserId + " has successfully Logged in");
                }
                
            });
            getConcertInfo();
        }
        else
        {
            Debug.Log("Authenticated");
            getConcertInfo();
        }
    }
	public static string fixUmlauts(string instring){
		Debug.Log ("instr " + instring);
		string outstr=instring;
		outstr=outstr.Replace(@"\u00e5", "å");
		outstr=outstr.Replace(@"\u00c5", "Å");
		outstr=outstr.Replace(@"\u00e4", "ä");
		outstr=outstr.Replace(@"\u00c4", "Ä");
		outstr=outstr.Replace(@"\u00f6", "ö");
		outstr=outstr.Replace(@"\u00d6", "Ö");
		Debug.Log ("outstr " + outstr);
		return outstr;
	}

    void getConcertInfo()
    {
        new GetPropertyRequest().SetPropertyShortCode("coninfo").Send((response) =>
        {
            Debug.Log("Concert Information - " + response.Property.JSON.Remove(0, 11).TrimEnd('"', '}'));
            concertInfo = response.Property.JSON.Remove(0, 11).TrimEnd('"', '}');
			concertInfo=fixUmlauts(concertInfo);
            PlayerPrefs.SetString("ConcertInfo", concertInfo);
        });
      
    }
}
