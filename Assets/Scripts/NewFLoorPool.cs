﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NewFLoorPool : MonoBehaviour
{

    public GameObject[] objectToSpawn;
    public static NewFLoorPool current;
    private int randomPick;
    public int noOfObjects = 10;
    private GameObject obj;

    //private List<GameObject> backList;
    private List<GameObject> midList;
    private List<GameObject> frontList;
    void Awake()
    {
        current = this;
      //  backList = new List<GameObject>();
        midList = new List<GameObject>();
        frontList = new List<GameObject>();
    }

    void Start()
    {

        //for (int i = 0; i < noOfObjects; i++)
        //{
            
        //    GameObject instance = Instantiate(objectToSpawn[0]);
        //    instance.SetActive(false);
        //    backList.Add(instance);
        //}
        for (int i = 0; i < noOfObjects; i++)
        {
           
            GameObject instance = Instantiate(objectToSpawn[1]);
            instance.SetActive(false);
            midList.Add(instance);
        }
        for (int i = 0; i < noOfObjects; i++)
        {
            
            GameObject instance = Instantiate(objectToSpawn[2]);
            instance.SetActive(false);
            frontList.Add(instance);
        }
    }

    //public GameObject GetBackground()
    //{
    //    for (int i = 0; i < backList.Count; i++)
    //    {

    //        if (!backList[i].activeInHierarchy)
    //        {
    //            return backList[i];
    //        }
    //    }
    //    GameObject obj = Instantiate(objectToSpawn[0]);
    //    backList.Add(obj);
    //    return obj;
    //}
    public GameObject GetMid()
    {
        for (int i = 0; i < midList.Count; i++)
        {

            if (!midList[i].activeInHierarchy)
            {
                return midList[i];
            }
        }
        GameObject obj = Instantiate(objectToSpawn[1]);
        midList.Add(obj);
        return obj;
    }
    public GameObject GetFront()
    {
        for (int i = 0; i < frontList.Count; i++)
        {

            if (!frontList[i].activeInHierarchy)
            {
                return frontList[i];
            }
        }
        GameObject obj = Instantiate(objectToSpawn[2]);
        frontList.Add(obj);
        return obj;
    }

    public List<GameObject> GetListOfObjects(GameObject[] objectToSpawn, int amount)
    {

        List<GameObject> theList = new List<GameObject>();

        for (int i = 0; i < amount; i++)
        {
            int randomPick = Mathf.Abs(Random.Range(0, this.objectToSpawn.Length));
            GameObject obj = Instantiate(objectToSpawn[randomPick]);
            theList.Add(obj);
        }
        return theList;
    }
}