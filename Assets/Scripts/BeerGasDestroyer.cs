﻿using UnityEngine;
using System.Collections;

public class BeerGasDestroyer : MonoBehaviour
{
    private SpriteRenderer sr;
    public Sprite normal;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }
    void OnBecameInvisible()
    {
        sr.sprite = normal;
        gameObject.SetActive(false);
    }
}
