﻿using UnityEngine;
using System.Collections;

public class GasTankController : MonoBehaviour {

    public Sprite gas0,gas5,gas10,gas15,gas20,gas25,gas30,gas35,gas40,gas45,gas50,gas55,gas60,gas65,gas70,gas75,gas80,gas85,gas90,gas95,gas100;
    private SpriteRenderer spriteRenderer;

    void Awake()
    {

    }
	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void LateUpdate()
    {
       GasTank();
    }

    void GasTank()
    {
        switch (DemoScene.GasTankCounter)
        {
            case 1:
                spriteRenderer.sprite = gas5;
                break;
            case 2:
                spriteRenderer.sprite = gas10;
                break;
            case 3:
                spriteRenderer.sprite = gas15;
                break;
            case 4:
                spriteRenderer.sprite = gas20;
                break;
            case 5:
                spriteRenderer.sprite = gas25;
                break;
            case 6:
                spriteRenderer.sprite = gas30;
                break;
            case 7:
                spriteRenderer.sprite = gas35;
                break;
            case 8:
                spriteRenderer.sprite = gas40;
                break;
            case 9:
                spriteRenderer.sprite = gas45;
                break;
            case 10:
                spriteRenderer.sprite = gas50;
                break;
            case 11:
                spriteRenderer.sprite = gas55;
                break;
            case 12:
                spriteRenderer.sprite = gas60;
                break;
            case 13:
                spriteRenderer.sprite = gas65;
                break;
            case 14:
                spriteRenderer.sprite = gas70;
                break;
            case 15:
                spriteRenderer.sprite = gas75;
                break;
            case 16:
                spriteRenderer.sprite = gas80;
                break;
            case 17:
                spriteRenderer.sprite = gas85;
                break;
            case 18:
                spriteRenderer.sprite = gas90;
                break;
            case 19:
                spriteRenderer.sprite = gas95;
                break;
            case 20:
                spriteRenderer.sprite = gas100;
                break;
            default:
                spriteRenderer.sprite = gas0;
                break;
        }
    }
}
