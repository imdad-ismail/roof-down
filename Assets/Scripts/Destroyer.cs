﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {
    private SpriteRenderer sr;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }
    void OnBecameInvisible()
    {
        sr.material.color = Color.white;
        gameObject.SetActive(false);
    }
}
