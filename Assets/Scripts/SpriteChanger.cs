﻿using UnityEngine;
using System.Collections;

public class SpriteChanger : MonoBehaviour {
    private SpriteRenderer sr;
    public Sprite Normal;//The normal sprite without any white outline
    public Sprite Feedback;//The Feedback sprite with white outline
                          
    void Start () {
        sr = this.gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D other)
    {
          if (other.gameObject.layer == 8)
            {
                sr.sprite = Feedback;
            }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.layer == 8)
        {
          
        }

    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == 8)
        {
            sr.sprite = Normal;
        }

    }
}
