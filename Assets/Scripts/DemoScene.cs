﻿using UnityEngine;
using System.Collections;
using Prime31;


public class DemoScene : MonoBehaviour
{
    // movement config
    public float gravity = -25f;
    public float runSpeed = 8f;
    public float groundDamping = 20f; // how fast do we change direction? higher means faster
    public float inAirDamping = 5f;
    public float jumpHeight = 3f;


    [HideInInspector]
    private float normalizedHorizontalSpeed = 0;
    private CharacterController2D _controller;
    private Animator _animator;
    private RaycastHit2D _lastControllerColliderHit;
    private Vector3 _velocity;
    public static int GasTankCounter;
    public static int countGasPicks;
    public static int countBeerPicks;
    public static int beerTankCounter;
    public static bool triggerStay;//Validating Human obstacle has been avoided.
    private int videoCounter;
    private bool jump;//Validating swipe up
    private bool doOnce;//Making sure the function only runs once onTriggerStay for all obstacles
    private bool doOnceTv;//Making sure the function only runs once onTriggerStay for TV
    private bool doOnceBeer;//Making sure the function only runs once onTriggerStay for beer
    private bool doOnceGas;//Making sure the function only runs once onTriggerStay for gas
    private float fingerStartTime = 0.0f;
    private Vector2 fingerStartPos = Vector2.zero;
    private bool isSwipe = false;
    private float minSwipeDist = 50.0f;
    private float maxSwipeTime = 0.5f;
    public static bool swipeDetected;//Validating Touch on human obstacles gets the value from Spriteflasher.
    public static bool MissedObs;//Validating missed human obstacles
    public static bool HitSObs; //Validating Hits on small obstacles
    public static bool gasPicked;//Validating when gas is picked up
    public static bool tvPicked;//Validating when TV is picked up
    public static bool beerPicked;//Validating when Beer is picked
    private bool picked;//Validating all pick ups
    private AudioSource gasPickupSound;
    private AudioSource obstacleMissSound;
    private AudioSource obstacleEvadeSound;
    private AudioSource hitObsSound;
    private AudioSource beerPickupSound;

    //  public GameObject gasPickup;

    void Awake()
    {

        
        _animator = GetComponent<Animator>();
        _controller = GetComponent<CharacterController2D>();

        // listen to some events for illustration purposes
        _controller.onControllerCollidedEvent += onControllerCollider;
        _controller.onTriggerEnterEvent += onTriggerEnterEvent;
        _controller.onTriggerExitEvent += onTriggerExitEvent;
        _controller.onTriggerStayEvent += onTriggerStayEvent;
        GasTankCounter = 0;
        beerTankCounter = 20;

    }
    void Start()
    {
        countBeerPicks = 0;
        countGasPicks = 0;
        videoCounter = 1;
        beerPicked = false;
        tvPicked = false;
        gasPicked = false;
        HitSObs = false;
        MissedObs = false;
        triggerStay = false;
        swipeDetected = false;
        doOnceGas = false;
        doOnceBeer = false;
        doOnceTv = false;
        AudioSource[] audios = GetComponents<AudioSource>();
        gasPickupSound = audios[0];
        obstacleMissSound = audios[1];
        obstacleEvadeSound = audios[2];
        hitObsSound = audios[3];
        beerPickupSound = audios[4];
    }



    #region Event Listeners

    void onControllerCollider(RaycastHit2D hit)
    {
        // bail out on plain old ground hits cause they arent very interesting
        if (hit.normal.y == 1f)
            return;

        //logs any collider hits if uncommented.
        //Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
    }


    void onTriggerEnterEvent(Collider2D col)
    {     
        if (col.gameObject.layer == 16 || col.gameObject.layer == 15)
        {
            doOnce = true;
            swipeDetected = false;
            triggerStay = false;
            
        }

        if (col.gameObject.layer == 18)
        {
            //Debug.Log("BEEERRRR DETECTED");
            picked = false;
            doOnceBeer = true;
            doOnceGas = true;           
        }
        if (col.gameObject.layer == 19)
        {
            doOnceTv = true;
            picked = false;
        }
        
        if (col.gameObject.layer == 15)
        {//normal obstacles
            hitObsSound.Play();
            HitSObs = true;
            beerTankCounter += -1;
        }
        
    }
  
    void VideoPlayer()
    {
        /*
        switch (videoCounter)
        {
            case 1:
                Handheld.PlayFullScreenMovie("video_1.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
                break;
            case 2:
                Handheld.PlayFullScreenMovie("video_2.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
                break;
            case 3:
                Handheld.PlayFullScreenMovie("video_3.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
                break;
            case 4:
                Handheld.PlayFullScreenMovie("video_4.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
                break;
            case 5:
                Handheld.PlayFullScreenMovie("video_5.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
                break;
            case 6:
                Handheld.PlayFullScreenMovie("video_6.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
                break;
            default:
                Handheld.PlayFullScreenMovie("video_1.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
                break;
        }
            
        videoCounter+=1;
        if(videoCounter>6)
        {
            videoCounter = 1;
        }*/
        VideoListManager.instance.PlayVideo();
    }
 
    void onTriggerStayEvent(Collider2D col)
    {      
        if (picked == true && col.gameObject.name == "Beer(Clone)" && doOnceBeer == true)
        {//Beer
            countBeerPicks += 1;
            beerTankCounter += 1;
            beerPickupSound.Play();
            Debug.Log("Beer detected");
            gasPicked = true;
            picked = false;
            doOnceBeer = false;
        }
        if (picked == true && col.gameObject.layer == 19 && doOnceTv == true && col.gameObject.name == "TV(Clone)")
        {//TV
            tvPicked = true;
            Debug.Log("TV detected");
            VideoPlayer();
            picked = false;
            doOnceTv = false;
        }
       
        if (swipeDetected == true && col.gameObject.layer == 16 && doOnce == true)
        {//Human Obstacles
            triggerStay = true;
            Debug.Log("Keypress detected");
            obstacleEvadeSound.Play();
            doOnce = false;
        }

        if (picked == true && col.gameObject.name == "Gas(Clone)" && doOnceGas == true )
        {// Gas pickup
                countGasPicks += 1;
                gasPicked = true;
                gasPickupSound.Play();
                GasTankCounter += 2;
                picked = false;
                doOnceGas = false;
        }
       
    }

	void onTriggerExitEvent( Collider2D col )
    {
       
        if (col.gameObject.layer == 16 && triggerStay == false && swipeDetected == false)
        {//HUman obstacles
            MissedObs = true;
            obstacleMissSound.Play();
            beerTankCounter += -10;            
        }
        if (col.gameObject.layer == 16 && triggerStay == true)
        {
            Debug.Log("Successfully EVADED");
        }

    }

	#endregion


	// the Update loop contains a very simple example of moving the character around and controlling the animation
	void Update()
	{
        if (Input.GetKey(KeyCode.RightArrow))
        {
            swipeDetected = true;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            picked = true;
        }


            if (SpawnPlayer.selectorIndex == 1)
        {//Johan selected
            if (_controller.isGrounded)
            {
                _velocity.y = 0;
                normalizedHorizontalSpeed = 1;
             
                if (transform.localScale.x < 0f)
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

                if (_controller.isGrounded)
                    _animator.Play(Animator.StringToHash("JohanWalk"));
            }

            //we can only jump whilst grounded
            if (_controller.isGrounded && Input.GetKeyDown(KeyCode.Space))
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
                _animator.Play(Animator.StringToHash("JohanJump"));
            }

            if (jump == true && _controller.isGrounded)
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
                _animator.Play(Animator.StringToHash("JohanJump"));
                jump = false;
            }
      }
        if (SpawnPlayer.selectorIndex == 2)
        {//Tobbe selected
            if (_controller.isGrounded)
            {
                _velocity.y = 0;
                normalizedHorizontalSpeed = 1;
                
                if (transform.localScale.x < 0f)
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

                if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("TobbeWalk"));
            }

            // we can only jump whilst grounded
            if (_controller.isGrounded && Input.GetKeyDown(KeyCode.Space))
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
               _animator.Play(Animator.StringToHash("TobbeJump"));
            }

            if (jump == true && _controller.isGrounded)
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
                _animator.Play(Animator.StringToHash("TobbeJump"));
              jump = false;
            }

        }
        if (SpawnPlayer.selectorIndex == 3)
        {//Staffan selected
            if (_controller.isGrounded)
            {
                _velocity.y = 0;
                normalizedHorizontalSpeed = 1;

                if (transform.localScale.x < 0f)
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

                if (_controller.isGrounded)
                    _animator.Play(Animator.StringToHash("StaffanWalk"));
            }

            // we can only jump whilst grounded
            if (_controller.isGrounded && Input.GetKeyDown(KeyCode.Space))
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
                _animator.Play(Animator.StringToHash("StaffanJump"));
            }

            if (jump == true && _controller.isGrounded)
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
                _animator.Play(Animator.StringToHash("StaffanJump"));
                jump = false;
            }

        }
        if (SpawnPlayer.selectorIndex == 4)
        {//Mats selected
            if (_controller.isGrounded)
            {
                _velocity.y = 0;
                normalizedHorizontalSpeed = 1;

                if (transform.localScale.x < 0f)
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

                if (_controller.isGrounded)
                    _animator.Play(Animator.StringToHash("MatsWalk"));
            }

            // we can only jump whilst grounded
            if (_controller.isGrounded && Input.GetKeyDown(KeyCode.Space))
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
                _animator.Play(Animator.StringToHash("MatsJump"));
            }

            if (jump == true && _controller.isGrounded)
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
                _animator.Play(Animator.StringToHash("MatsJump"));
                jump = false;
            }

        }
        if (SpawnPlayer.selectorIndex==0) 
        {//Tobbe selected
            if (_controller.isGrounded)
            {
                _velocity.y = 0;
                normalizedHorizontalSpeed = 1;
                if (transform.localScale.x < 0f)
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            }

            // we can only jump whilst grounded
            if (_controller.isGrounded && Input.GetKeyDown(KeyCode.Space))
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
            }

            if (jump == true && _controller.isGrounded)
            {
                _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
                jump = false;
            }

        }

        // apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
        var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		_velocity.x = Mathf.Lerp( _velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor );

		// apply gravity before moving
		_velocity.y += gravity * Time.deltaTime;


		_controller.move( _velocity * Time.deltaTime );

		// grab our current _velocity to use as a base for all calculations
		_velocity = _controller.velocity;

        
        if (Input.touchCount > 0)
        {

            foreach (Touch touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        /* this is a new touch */
                        isSwipe = true;
                        Debug.Log("Touch Began");
                        fingerStartTime = Time.time;
                        fingerStartPos = touch.position;
                        break;

                    case TouchPhase.Canceled:
                        /* The touch is being canceled */
                        isSwipe = false;
                        break;

                    case TouchPhase.Ended:

                        float gestureTime = Time.time - fingerStartTime;
                        float gestureDist = (touch.position - fingerStartPos).magnitude;

                        if (isSwipe && gestureTime < maxSwipeTime && gestureDist > minSwipeDist)
                        {
                            Vector2 direction = touch.position - fingerStartPos;
                            Vector2 swipeType = Vector2.zero;

                            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                            {
                                // the swipe is horizontal:
                                swipeType = Vector2.right * Mathf.Sign(direction.x);
                            }
                            else
                            {
                                // the swipe is vertical:
                                swipeType = Vector2.up * Mathf.Sign(direction.y);
                            }

                            if (swipeType.x != 0.0f)
                            {
                                if (swipeType.x > 0.0f)
                                {
                                    // Swipe RIGHT
                                 
                                }
                                else
                                {
                                  
                                    // Swipe LEFT
                                }
                            }

                            if (swipeType.y != 0.0f)
                            {
                                if (swipeType.y > 0.0f)
                                {
                                    jump = true;
                                    // Swipe UP
                                }
                                else
                                {
                                    picked = true;
                                    // Swipe DOWN
                                }
                            }

                        }
                        else
                        {
                           
                       }

                        break;
                }
            }
        }
      
     }
    IEnumerator MoveFromTo(Vector3 start, Vector3 end, float time)
    {

        float t = 0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime / time; // Sweeps from 0 to 1 in time seconds
            transform.position = Vector3.Lerp(start, end, t); // Set position proportional to t
            yield return 0;      // Leave the routine and return here in the next frame
        }
        
    }
}


   




