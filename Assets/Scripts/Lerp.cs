﻿using UnityEngine;
using System.Collections;

public class Lerp : MonoBehaviour {
    private Vector3 upwards;//Upwards movement for lerp

    
    // Use this for initialization
    void Start () {
       
	}
	
	// Update is called once per frame
	void Update ()
    {//|| DemoScene.beerPicked == true
        if (DemoScene.gasPicked == true )
        {//Used to lerp Gas and Beer
            upwards = new Vector3(transform.position.x+10, transform.position.y +10, transform.position.z+ 10);
            StartCoroutine(MoveFromTo(transform.position, upwards, 0.7f));
            DemoScene.gasPicked = false;           
        }	
	}
    IEnumerator MoveFromTo(Vector3 start, Vector3 end, float time)
    {
       
        Vector3 smallerGas = new Vector3(transform.localScale.x - 0.03f, transform.localScale.y - 0.03f, transform.localScale.z - 0.03f);
        float t = 0f;
        while (t < 1.0f)
           
        {
            t += Time.deltaTime / time; // Sweeps from 0 to 1 in time seconds
            transform.position = Vector3.Lerp(start, end, t); // Set position proportional to t
            transform.localScale = Vector3.Lerp(transform.localScale, smallerGas, t);
            yield return 0;      // Leave the routine and return here in the next frame
        }
       
    }
}
