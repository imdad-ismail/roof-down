﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public GameObject PausePanel;
	// Use this for initialization
	void Start () {
        PausePanel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void PauseButt ()
    {
        PausePanel.SetActive(true);
        Time.timeScale = 0;
    }
}
