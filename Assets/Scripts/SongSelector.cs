﻿using UnityEngine;
using System.Collections;

public class SongSelector : MonoBehaviour {
    private int songIndex;
	// Use this for initialization
	void Start () {
        songIndex = Random.Range(1,6);
        PlayerPrefs.SetInt("SongSelect", songIndex);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
