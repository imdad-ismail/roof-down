﻿using UnityEngine;
using System.Collections;

public class BGOverlapCheck : MonoBehaviour
{//Used to check for overlaps when spawning objects
    public float radius;
    public LayerMask collisionCheckMask;
    private Collider2D[] colliderArray;
    private Vector2 point;
    public static bool overlapDisable;
    private SpriteRenderer sr;
    public Sprite normal;
    // Use this for initialization
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        overlapDisable = false;
    }

    // Update is called once per frame
    void Update()
    {



    }
    void LateUpdate()
    {

        if (doesOverlap() == true)
        {
            if (overlapDisable == false)
            {
                sr.sprite = normal;
                gameObject.SetActive(false);
            }
        }
    }
    public bool doesOverlap()
    {
        if (colliderArray == null)
            colliderArray = new Collider2D[1];

        point.x = transform.position.x;
        point.y = transform.position.y;

        return Physics2D.OverlapCircleNonAlloc(point, radius, colliderArray, collisionCheckMask) > 0;
        //returning true or false depending on if it does or does not overlap
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 18)
        {
            overlapDisable = true;//Temporary overlap disable so it doesnt hit other colliders when lerping
        }

    }

}
