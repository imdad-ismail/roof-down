﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PickUpsPool : MonoBehaviour
{
    public GameObject TV;
    public GameObject[]GasBeer;
    public static PickUpsPool current;
    private int randomPick;
    public int noOfObjects;

    private List<GameObject> GasBeerList;
    private List<GameObject> TVList;

    void Awake()
    {
        current = this;
        GasBeerList = new List<GameObject>();
        TVList = new List<GameObject>();
    }

    void Start()
    {

        for (int i = 0; i < noOfObjects; i++)
        {
            //int randomPick = Mathf.Abs(Random.Range (0, this.objectToSpawn.Length));
            GameObject gasinstance = Instantiate(GasBeer[i]);
            gasinstance.SetActive(false);
            GasBeerList.Add(gasinstance);
        }
        for (int i = 0; i < noOfObjects; i++)
        {
            //int randomPick = Mathf.Abs(Random.Range (0, this.objectToSpawn.Length));
            GameObject tvinstance = Instantiate(TV);

            tvinstance.SetActive(false);
            TVList.Add(tvinstance);
        }
    }

    public GameObject GetObjectTV()
    {
        for (int i = 0; i < TVList.Count; i++)
        {

            if (!TVList[i].activeInHierarchy)
            {
                return TVList[i];
            }
        }
   
        GameObject tvObj = Instantiate(TV);

        TVList.Add(tvObj);
        return tvObj;
    }
    public GameObject GetObjGasBeer()
    {
        for (int i = 0; i < GasBeerList.Count; i++)
        {

            if (!GasBeerList[i].activeInHierarchy)
            {
                return GasBeerList[i];
            }
        }
        int randomPick = Mathf.Abs(Random.Range(0, this.GasBeer.Length));
        GameObject gasObj = Instantiate(GasBeer[randomPick]);

        GasBeerList.Add(gasObj);
        return gasObj;
    }

    //public List<GameObject> GetListOfObjects(GameObject[] objectToSpawn, int amount)
    //{

    //    List<GameObject> theList = new List<GameObject>();

    //    for (int i = 0; i < amount; i++)
    //    {
    //        int randomPick = Mathf.Abs(Random.Range(0, this.objectToSpawn.Length));
    //        GameObject obj = Instantiate(objectToSpawn[randomPick]);
    //        theList.Add(obj);


    //    }
    //    return theList;
    //}
}