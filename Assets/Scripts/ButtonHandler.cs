﻿using UnityEngine;
using System.Collections;

public class ButtonHandler : MonoBehaviour {
    public GameObject infoButt;
    public GameObject infoPanel;
    public GameObject pausePanel;
    public GameObject pauseButt;
	// Use this for initialization

	void Start () {
        infoPanel.SetActive(false);
        infoButt.SetActive(true);
        pauseButt.SetActive(true);
	}
	
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Application.loadedLevelName == "DemoScene" && !GameOverManager.gameover)
            {
                pauseMenu();
            }
            else if (Application.loadedLevelName == "DemoScene" && GameOverManager.gameover)
            {
                Application.LoadLevel("StartScene");
            }
        }
	}

    public void openInfo()
    {
        infoButt.SetActive(false);
        infoPanel.SetActive(true);
        Time.timeScale = 0f;        
    }
    public void closeInfo()
    {
        infoButt.SetActive(true);
        infoPanel.SetActive(false);
        pausePanel.SetActive(false);
        pauseButt.SetActive(true);
        Time.timeScale = 1;      
    }

    public void pauseMenu()
    {
        infoButt.SetActive(false);
        pauseButt.SetActive(false);
        pausePanel.SetActive(true);
        Time.timeScale = 0;
    }

    public void selectPlayer()
    {
        Time.timeScale = 1;
        Application.LoadLevel("StartScene");
    }

    public void restartGame ()
    {
        Time.timeScale = 1;
        Application.LoadLevel("DemoScene");
    }
}
