﻿using UnityEngine;
using System.Collections;

public class ObstacleSpawner : MonoBehaviour
{
    public float startDistance = 10;//Distance from the plane where objects start
    public float xDistance = 100;   //The distance where objects are instantiated
    public float minSpread = 5;     //The min Spread of objects
    public float maxSpread = 10;    //The max Spread of objects
    public float lanePos = 0;       //Y position of the object
    private Transform playerTransform;
    
    private GameObject player;
    private GameObject Obs;
    //public GameObject[] objects;

    float xSpread;
    float lastxPos;
    // Use this for initialization
    void Awake()
    {
       
    }
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerTransform = player.transform;
            //GetComponent<Transform>();
        lastxPos = playerTransform.position.x + (startDistance - xSpread - xDistance);
    }
    
    // Update is called once per frame
    void Update()
    {
        if (playerTransform.position.x - lastxPos >= xSpread)
        {
            SpawnObstacles();

        }
      
    }
    void SpawnObstacles()
    {
        
        Obs = ObstaclePoolScript.current.GetObject();
        Obs.name = "Obstacles";
        //float lanePos = (Random.Range(4, 11)); //Lane Position on the Y axis
        //lanePos = (lanePos - 1) * 1.5f;
        lastxPos += xSpread;
        xSpread = Random.Range(minSpread, maxSpread);

        if (Obs != null)
        {
            
            Obs.transform.position = new Vector3(lastxPos + xSpread + xDistance, lanePos, 100);
            Obs.SetActive(true);

        }


    }


}
