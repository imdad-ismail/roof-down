﻿using UnityEngine;
using System.Collections;

public class FloorSpawner : MonoBehaviour
{
    public float startDistBack, startDistMid, startDistFront; //Distance from the plane where objects start
    public float xDistanceB, xDistancM, xDistanceF;    //The distance where objects are instantiated
    public float spreadB, spreadM, spreadF;
    public float lanePosB, lanePosM, lanePosF;       //Y position of the object

    private Transform playerTransform;
    private Transform playerTransformB;

    public static int SceneSelector;
    public GameObject front, middle, back;
    public Sprite desFront, desMiddle, desBack;
    public Sprite dumpFront, dumpMiddle, dumpBack;
    public Sprite forFront, forMiddle, forBack;
    public Sprite scaryFront, scaryMiddle, scaryBack;

    private GameObject background;
    private GameObject mid;
    private GameObject floor;
    private int counter;

    private SpriteRenderer frSprite, mSprite, bSprite;
    private GameObject player;
   // Vector3 StartPos;
    float lastxPosB, lastxPosM, lastxPosF,newLastXPosB;

    void Awake()
    {
        frSprite = front.GetComponent<SpriteRenderer>();
        mSprite  = middle.GetComponent<SpriteRenderer>();
        bSprite  = back.GetComponent<SpriteRenderer>();

        SceneSelector = Random.Range(1, 5);
        Debug.Log(SceneSelector);
        spriteChanger();
    }
    void Start()
    {
       // StartPos = new Vector3(0, 0, 0);
        player = GameObject.FindGameObjectWithTag("Player");
        playerTransform = player.transform;
       
        lastxPosB = playerTransform.position.x + (startDistBack - spreadB - xDistanceB);
        lastxPosM = playerTransform.position.x + (startDistMid - spreadM - xDistancM);
        lastxPosF = playerTransform.position.x + (startDistFront - spreadF - xDistanceF);

  

    }
    
    // Update is called once per fram
    void Update()
    {
       
        if (playerTransform.position.x - lastxPosB >= spreadB)
        {
            //SpawnBack();
            //SpawnFront();
            //SpawnMid();
        }
       
        if (playerTransform.position.x - lastxPosM >= spreadM)
        {
            //SpawnFront();
            SpawnMid();
            //SpawnBack();
        }
        if (playerTransform.position.x - lastxPosF >= spreadF)
        {
            SpawnFront();
            //SpawnMid();
            //SpawnBack();
        }


    }
    void SpawnFront()
    {
        floor = NewFLoorPool.current.GetFront();
       
        lastxPosF += spreadF;
        
        if (floor != null)
        {
            floor.transform.position = new Vector3(lastxPosF + spreadF + xDistanceF , lanePosF, 108 );
            floor.SetActive(true);
        }


    }
    void SpawnMid()
    {
        mid = NewFLoorPool.current.GetMid();
        //floor.name = "Parallax";
        lastxPosM += spreadM;

        if (mid != null)
        {
            mid.transform.position = new Vector3(lastxPosM + spreadM + xDistancM, lanePosM, 109);
            mid.SetActive(true);
        }
    }
    //void SpawnBack()
    //{
       
    //    background = NewFLoorPool.current.GetBackground();
    //    newLastXPosB += spreadB;

    //    if (background != null)
    //    {
            
    //        background.transform.position = new Vector3(newLastXPosB + spreadB, lanePosB, 111);
    //        newLastXPosB = background.transform.position.x;
    //        background.SetActive(true);
    //    }
    //}
    void spriteChanger()
    { 
        switch (SceneSelector)
        {
            case 1:
                frSprite.sprite = desFront;
                mSprite.sprite = desMiddle;
                bSprite.sprite = desBack;
                break;
            case 2:
                frSprite.sprite = dumpFront;
                mSprite.sprite = dumpMiddle;
                bSprite.sprite = dumpBack;
                break;
            case 3:
                frSprite.sprite = forFront;
                mSprite.sprite = forMiddle;
                bSprite.sprite = forBack;
                break;
            case 4:
                frSprite.sprite = scaryFront;
                mSprite.sprite = scaryMiddle;
                bSprite.sprite = scaryBack;
                break;

            default:
                frSprite.sprite = desFront;
                mSprite.sprite = desMiddle;
                bSprite.sprite = desBack;
                break;

        }
       
    }

}
