﻿using UnityEngine;
using System.Collections;

public class CloudSpawner : MonoBehaviour
{

    private Transform playerTransform;

    private GameObject player;
    private GameObject clouds;

    void Awake()
    {

    }
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerTransform = player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (DemoScene.MissedObs == true)
        {
            SpawnCloud();
            
        }
        if (DemoScene.HitSObs == true)
        {
            SpawnSmallCloud();
            DemoScene.HitSObs = false;
        }
    }
    void SpawnCloud()
    {

        clouds = CloudPool.current.GetObject();

        if (clouds != null)
        {
            clouds.transform.localScale = new Vector3(playerTransform.localScale.x+0.8f, playerTransform.localScale.y + 0.8f, playerTransform.localScale.z + 0.8f);
            clouds.transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y+1, 99);
            clouds.SetActive(true);
            Invoke("DisableCloud", 0.3f);
        }
        DemoScene.MissedObs = false;

    }
    void SpawnSmallCloud()
    {

        clouds = CloudPool.current.GetObject();

        if (clouds != null)
        {
            clouds.transform.localScale = new Vector3(playerTransform.localScale.x, playerTransform.localScale.y , playerTransform.localScale.z);
            clouds.transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y+0.5f, 99);
            clouds.SetActive(true);
            Invoke("DisableCloud", 0.3f);
        }


    }
    void DisableCloud()
    {
        clouds.SetActive(false);
    }
}
