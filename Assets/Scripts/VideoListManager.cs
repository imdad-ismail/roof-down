﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VideoListManager : MonoBehaviour {

    //Reference for VideoListManager instance
    public static VideoListManager instance;

    private List<int> remainingVideos;
    private int currentVideo;

	void Awake ()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;

        } else if (instance != this)
        {
            Destroy(gameObject);
        }

        remainingVideos = new List<int>();

        for (int i = 1; i < 7; i++ )
        {
            remainingVideos.Add(i);
        }
    }

    public void PlayVideo ()
    {
        if (remainingVideos.Count <= 0)
        {
            for (int i = 1; i < 7; i++)
            {
                remainingVideos.Add(i);
            }
        }

        int videoId = Random.Range(0, remainingVideos.Count);
        currentVideo = remainingVideos[videoId];
        remainingVideos.RemoveAt(videoId);
        Handheld.PlayFullScreenMovie("video_" + currentVideo + ".mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
    }
}
