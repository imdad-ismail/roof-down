﻿using UnityEngine;
using System.Collections;

public class GasSpawner : MonoBehaviour {
    public float startDistance = 10;//Distance from the plane where objects start
    public float xDistance = 100;//The distance where objects are instantiated
    public float minSpread = 5;//The min Spread of objects
    public float maxSpread = 10;//The max Spread of objects
    public float tvSpread; // The spread of TV
    public float lanePos = 0;//Y position of the object

    private GameObject player;
    private GameObject tv;
    private GameObject gasBeer;
    private Transform playerTransform;
    //Collider2D gasCollider;
    //public GameObject[] objects;
    float lastxPosTv;
    float xSpread;
    float lastxPos;
    // Use this for initialization
    void Awake()
    {
       
    }
    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerTransform = player.transform;
        lastxPos = playerTransform.position.x + (startDistance - xSpread - xDistance);
        lastxPosTv = playerTransform.position.x + (startDistance  -xSpread  -xDistance);
    }

    // Update is called once per frame
    void Update () {
        if (playerTransform.position.x - lastxPos >= xSpread)
        {
            SpawnGas();

        }
        if (playerTransform.position.x - lastxPosTv >= tvSpread)
        {
            SpawnTV();
        }
    }
    void SpawnGas()
    {
        gasBeer = PickUpsPool.current.GetObjGasBeer();
        lastxPos += xSpread;
        xSpread = Random.Range(minSpread, maxSpread);
        OverlapChecker.overlapDisable = false;

        if (gasBeer != null)
        {
            gasBeer.GetComponent<SpriteRenderer>().enabled = true;
            gasBeer.transform.localScale = new Vector3(0.074f, 0.074f, 0.074f);
            gasBeer.transform.position = new Vector3(lastxPos + xSpread + xDistance, lanePos, 100);
            gasBeer.SetActive(true);
        }


    }
    void SpawnTV()
    {       
        tv = PickUpsPool.current.GetObjectTV();
        lastxPosTv += tvSpread;

        if (tv != null)
        {
            tv.transform.position = new Vector3(lastxPos + tvSpread + xDistance, lanePos, 100);
            tv.SetActive(true);
        }


    }
}
