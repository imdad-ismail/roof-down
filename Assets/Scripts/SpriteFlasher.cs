﻿using UnityEngine;
using System.Collections;

public class SpriteFlasher : MonoBehaviour {
    private SpriteRenderer sr;
    public float flashSpeed;
    public static bool doOnceE;// Making sure only one exclamation is spawned per obstacle
    private BoxCollider2D col2D;
    private GameObject excl; // Exclamation Game object
 

    // Use this for initialization
    void Start () {
        sr = GetComponent<SpriteRenderer>();
        col2D = GetComponent<BoxCollider2D>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.touchCount >0)
        {//Checking for touches on the human obstacles
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
          
            if (col2D.OverlapPoint(wp))
            {
                DemoScene.swipeDetected = true;
            }
          
        }

        if (RangeDetector.inCollider == true && doOnceE == false)
        {
            Debug.Log("Exclamation Spawned");
            SpawnExclamation();
            doOnceE = true;
        }
     
       
     
    }

    IEnumerator WaitAndFlash()
    {
        
        while (RangeDetector.inCollider == true)
        {
            sr.material.color = Color.white;
            yield return new WaitForSeconds(flashSpeed);
            sr.material.color = Color.blue;
            yield return new WaitForSeconds(flashSpeed);
     
        }
        sr.material.color = Color.white;

    }
    void OnTriggerEnter2D(Collider2D other)
    {

    }

    void OnTriggerExit2D(Collider2D other)
    {       
        sr.material.color = Color.white;
    }
    void SpawnExclamation()
    {
        excl = ObstaclePoolScript.current.GetEx();

        if (excl != null)
        {
            excl.transform.position = new Vector3(transform.position.x, transform.position.y + 2.5f, 99);
            excl.SetActive(true);
        }
    }

}
